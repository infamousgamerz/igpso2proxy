﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGPSO2PROXY.Classes
{
    public class UserSession
    {
        public string ip_addr { get; set; }
        public int port { get; set; }
        public byte[] client_rc4 { get; set; }
        public byte[] sega_rc4 { get; set; }
    }
}
