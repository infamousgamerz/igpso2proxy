﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace IGPSO2PROXY.Classes
{
    public class Packets
    {
        //UserSession
        UserSession session;
        //Shipserver
        Server socketServer;

        //Handshake
        public bool handshake(byte[] packet)
        {
            Functions functions = new Functions();
            byte[] handshake_packet_flag = { 0x11, 0x0B, 0x00, 0x00 };
            bool result = CheckFlag(packet, handshake_packet_flag);
            if (result == true)
            {
                return true;
            }
            return false;
        }

        //LoadBalancer hack
        public void login_packet_fix(byte[] packet)
        {
            Functions functions = new Functions();
            byte[] login_packet_flag = { 0x11, 0x2C, 0x00, 0x00 };
            bool result = CheckFlag(packet, login_packet_flag);
            if (result == true)
            {
                //Start socket server based on IP and PORT from SEGA packet
                var ip_port_bytes = packet.Skip(104).Take(6).ToArray();
                var str_arr = ip_port_bytes.Take(4).Select(m => m.ToString());
                string ip_address = string.Join(".", str_arr);
                Int16 port = BitConverter.ToInt16(ip_port_bytes, 4);
                session = new UserSession();
                socketServer = new Server(session);
                socketServer.RemoteAddress = ip_address;
                socketServer.LocalPort = port;
                socketServer.RemotePort = port;
                socketServer.Start();
                functions.write_to_log("Ship server started at: " + ip_address + ":" + port);

                //Login packet fix to 127.0.0.1
                int ip_offset = 104;
                byte[] login_packet_ip_replace = { 0x7F, 0x00, 0x00, 0x01 };
                this.WriteBytes(packet, ip_offset, login_packet_ip_replace);

                //RC4 key
                this.sega_rsa_handshake(session);

                //Send the packet to the server..
                //socketServer.m_vClient.SendToServer(packet);
            }
        }

        public byte[] rsa_handshake(UserSession new_session, byte[] packet)
        {
            //RSA Key
            string encrpytion_key_dir = IGPSO2PROXY.Classes.Functions.Globals.keys_dir;
            string encryption_key = (encrpytion_key_dir + "privatekey.blob");
            var get_bytes_from_blob = File.ReadAllBytes(encryption_key);
            
            //Create a new instance of RSACryptoServiceProvider.
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSA.ImportCspBlob(get_bytes_from_blob);

            //Packet reverse
            byte[] payload = packet.Skip(8).Take(0x80).Reverse().ToArray();

            //Decrypt
            RSAPKCS1KeyExchangeDeformatter keyFormatter = new RSAPKCS1KeyExchangeDeformatter(RSA);
            byte[] rc4key = keyFormatter.DecryptKeyExchange(payload);

            //Save Client RSA to session
            session = new_session;
            session.client_rc4 = rc4key;

            return rc4key;
        }

        public byte[] sega_rsa_handshake(UserSession new_session)
        {
            //RSA Key
            string encrpytion_key_dir = IGPSO2PROXY.Classes.Functions.Globals.keys_dir;
            string encryption_key = (encrpytion_key_dir + "segakey.blob");
            var get_bytes_from_blob = File.ReadAllBytes(encryption_key);

            //SEGA RSA Packet
            RC4 rc4crypt = new RC4();
            byte[] segarsa = rc4crypt.GenerateKey();

            //Save SEGA RSA to session
            session = new_session;
            session.sega_rc4 = segarsa;

            //Create a new instance of RSACryptoServiceProvider.
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSA.ImportCspBlob(get_bytes_from_blob);

            //Decrypt
            RSAPKCS1KeyExchangeFormatter keyFormatter = new RSAPKCS1KeyExchangeFormatter(RSA);
            byte[] rc4key = keyFormatter.CreateKeyExchange(segarsa);
            Functions functions = new Functions();
            string segarc4keylog = convert_byte_to_string(session.sega_rc4);
            functions.write_to_log("SEGA RC4 Key: " + segarc4keylog);
            return rc4key;
        }

        public void sega_rsa_handshake_packet(UserSession new_session)
        {
            //SEGA RSA Packet
            session = new_session;
            byte[] sega_rsa_packet_headers = { 0x0C, 0x01, 0x00, 0x00, 0x11, 0x0B, 0x00, 0x00 };
            var sega_rsa_packet = new byte[sega_rsa_packet_headers.Length + session.sega_rc4.Length];
            sega_rsa_packet_headers.CopyTo(sega_rsa_packet, 0);
            session.sega_rc4.CopyTo(sega_rsa_packet, sega_rsa_packet_headers.Length);

            //Write to log
            Functions functions = new Functions();
            string segarc4packetlog = convert_byte_to_string(sega_rsa_packet);
            functions.write_to_packet_log("SEGA RC4 Packet: " + segarc4packetlog);

            //Do RC4 encryption here
            RC4 rc4 = new RC4();
            rc4.Decrypt(session.sega_rc4, sega_rsa_packet);
            rc4.Encrypt(session.sega_rc4, sega_rsa_packet);

            //Send the packet to the server..
            //socketServer.m_vClient.SendToClient(sega_rsa_packet);
        }


        //Welcome packet
        public byte[] pso2_welcome()
        {
            Functions functions = new Functions();
            int chat_offset = 9999;
            string welcome_message = "Welcome to IGPSO2PROXY!  You are now connected.";
            byte[] welcome_message_bytes = Encoding.ASCII.GetBytes(welcome_message);
            byte[] welcome_packet = { 0x99, 0x99, 0x99, 0x99 };
            this.WriteBytes(welcome_packet, chat_offset, welcome_message_bytes);
            return welcome_packet;
        }

        //Sends designated packet from command in PSO2
        public byte[] pso2_commands(byte[] packet)
        {
            Functions functions = new Functions();
            byte[] packet_flag = { 0x99, 0x99, 0x99, 0x99 }; //Chat flag
            bool result = CheckFlag(packet, packet_flag);
            if (result == true)
            {
                //Chat position of packets in PSO2
                int chat_pos = 4;
                //Send a packet if command from PSO2 is triggered
                string packet_command = "$test"; //Command run in pso2 to trigger packet
                byte[] packet_command_bytes = Encoding.ASCII.GetBytes(packet_command);
                bool command_result = CheckOffset(packet, chat_pos, packet_command_bytes);
                if (command_result == true)
                {
                    byte[] packet_to_be_sent = { 0x99, 0x99, 0x99, 0x99 };  //Replace this with packet that you want to send
                    return packet_to_be_sent;
                }
                //End PSO2 command
            }
            return packet;
        }

        //Write Bytes to packet
        public void WriteBytes(byte[] p, int offset, byte[] b)
        {
            if (offset + b.Length >= p.Length)
                return;
            for (int i = 0; i < b.Length; i++)
                p[i + offset] = b[i];
        }

        //Convert byte to string
        public string convert_byte_to_string(byte[] bytes)
        {
            string packet_string_hex = "0x" + BitConverter.ToString(bytes).Replace("-", " 0x");
            return packet_string_hex;
        }

        //Check packet flag
        public bool CheckFlag(byte[] packet, byte[] flag)
        {
            int flag_pos = 4;
            if (packet.Length < flag_pos + flag.Length)
                return false;
            for (int i = 0; i < flag.Length; i++)
                if (packet[flag_pos + i] != flag[i])
                    return false;
            return true;
        }

        //Check packet offset
        public bool CheckOffset(byte[] packet, int offset, byte[] flag)
        {
            if (packet.Length < offset + flag.Length)
                return false;
            for (int i = 0; i < flag.Length; i++)
                if (packet[offset + i] != flag[i])
                    return false;
            return true;
        }
    }
}
