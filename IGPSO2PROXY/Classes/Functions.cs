﻿// -----------------------------------------------------------------------
// <copyright file="Functions.cs" company="">
// (c) InfamousNugz 2016 - IGPSO2PROXY
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IGPSO2PROXY.Classes
{
    public class Functions
    {
        /// Log and Key Check
        public static class Globals
        {
            public static String log_dir = Directory.GetCurrentDirectory() + @"\log\";
            public static String keys_dir = Directory.GetCurrentDirectory() + @"\keys\";
        }

        //Key check 1
        public void key_check_1()
        {
            string curFile1 = Globals.keys_dir + "segakey.blob";
            string key1 = File.Exists(curFile1) ? "segakey.blob file exists." : "segakey.blob file does not exist.";
            this.write_to_log(key1);
        }

        //Key check 2
        public void key_check_2()
        {
            string curFile2 = Globals.keys_dir + "publickey.blob";
            string key2 = File.Exists(curFile2) ? "publickey.blob file exists." : "publickey.blob file does not exist.";
            this.write_to_log(key2);
        }

        //Key check 3
        public void key_check_3()
        {
            string curFile2 = Globals.keys_dir + "privatekey.blob";
            string key2 = File.Exists(curFile2) ? "privatekey.blob file exists." : "privatekey.blob file does not exist.";
            this.write_to_log(key2);
        }

        //Write to log
        public void write_to_log(string data)
        {
            var log = data + Environment.NewLine;
            File.AppendAllText(Globals.log_dir + "log.txt", log);
        }

        //Write to packet log
        public void write_to_packet_log(string data)
        {
            var log = data + Environment.NewLine;
            File.AppendAllText(Globals.log_dir + "packet_log.txt", log);
        }
    }
}
