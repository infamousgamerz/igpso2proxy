﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using IGPSO2PROXY.Classes;

namespace IGPSO2PROXY
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string packet_log_file { get; set; }
        Queue<string> packet_log2_queue = new Queue<string>();
        private string log_file { get; set; }
        Queue<string> log2_queue = new Queue<string>();
        Server loadBalancer;
        Server shipServer;
        UserSession session;

        private void Form1_Load(object sender, EventArgs e)
        {
            //Servers
            session = new UserSession();
            loadBalancer = new Server(session);
            loadBalancer.RemoteAddress = "210.189.208.16";
            loadBalancer.LocalPort = 12200;
            loadBalancer.RemotePort = 12200;
            loadBalancer.Start();

            shipServer = new Server(session);
            shipServer.RemoteAddress = "210.189.208.16";
            shipServer.LocalPort = 12199;
            shipServer.RemotePort = 12199;
            shipServer.Start();

            //Clear logs on load
            File.WriteAllText(Directory.GetCurrentDirectory() + @"\log\log.txt", String.Empty);
            File.WriteAllText(Directory.GetCurrentDirectory() + @"\log\packet_log.txt", String.Empty);

            //Key check
            Functions functions = new Functions();
            functions.key_check_1();
            functions.key_check_2();
            functions.key_check_3();

            //Log files
            log_file = Path.Combine(Directory.GetCurrentDirectory(), "log");
            packet_log_file = Path.Combine(log_file, "packet_log.txt");
            log_file = Path.Combine(log_file, "log.txt");
            richTextBox1.LoadFile(packet_log_file, RichTextBoxStreamType.PlainText);
            richTextBox1.ScrollToCaret();
            richTextBox2.LoadFile(log_file,RichTextBoxStreamType.PlainText);
            richTextBox2.ScrollToCaret();
            this.LogWatch();
            this.PacketLogWatch();
        }

        //Log Watch
        public void LogWatch() 
        {
            var watch = new FileSystemWatcher();
            watch.Path = Path.GetDirectoryName(log_file);
            watch.Filter = Path.GetFileName(log_file);
            watch.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite; //more options
            watch.Changed += new FileSystemEventHandler(OnChanged);
            watch.EnableRaisingEvents = true;
        }

        //Packet log watch
        public void PacketLogWatch()
        {
            var watch = new FileSystemWatcher();
            watch.Path = Path.GetDirectoryName(packet_log_file);
            watch.Filter = Path.GetFileName(packet_log_file);
            watch.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite; //more options
            watch.Changed += new FileSystemEventHandler(OnChanged);
            watch.EnableRaisingEvents = true;
        }

        //On log change
        public void OnChanged(object source, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Changed)
                return;

            bool fileIsLocked = false;
            do
            {
                try
                {
                    //Console log
                    using (var stream = File.Open(log_file, FileMode.Open, FileAccess.ReadWrite))
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            string new_entries = sr.ReadToEnd();
                            log2_queue.Enqueue(new_entries);
                            fileIsLocked = false;
                        }
                    }
                    //Packet log
                    using (var stream = File.Open(packet_log_file, FileMode.Open, FileAccess.ReadWrite))
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            string new_entries = sr.ReadToEnd();
                            packet_log2_queue.Enqueue(new_entries);
                            fileIsLocked = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileIsLocked = true;
                    System.Threading.Thread.Sleep(30);
                }
            } while (fileIsLocked);

        }

        //Add to console log
        private void AddToLog(string entry)
        { 
            richTextBox2.Clear();
            richTextBox2.AppendText(entry);
            richTextBox2.ScrollToCaret();
        }

        //Add to packet log
        private void AddToPacketLog(string entry)
        {
            richTextBox1.Clear();
            richTextBox1.AppendText(entry);
            richTextBox1.ScrollToCaret();
        }

        //Log timer
        private void log_tmr_Tick(object sender, EventArgs e)
        {
            string last_entry = "";
            //Console
            while(log2_queue.Count > 0)
            {
                var entry = log2_queue.Dequeue();
                if (string.IsNullOrEmpty(last_entry))
                {
                    last_entry = entry;
                    AddToLog(entry);
                }
                else if (!string.Equals(last_entry, entry))
                {
                    last_entry = entry;
                    AddToLog(entry);
                }
            }
            //Packet
            while (packet_log2_queue.Count > 0)
            {
                var entry = packet_log2_queue.Dequeue();
                if (string.IsNullOrEmpty(last_entry))
                {
                    last_entry = entry;
                    AddToPacketLog(entry);
                }
                else if (!string.Equals(last_entry, entry))
                {
                    last_entry = entry;
                    AddToPacketLog(entry);
                }
            }
        }

        //IG Link
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://infamousgamerz.net");
            Process.Start(sInfo);
        }

        //Credits
        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            String credit_text = "IGPSO2PROXY was built by InfamousNugz.\nTo learn more or get help please visit http://infamousgamerz.net\n\nThanks:\nSpecial thanks to Sziadan for all his help on this project. http://pso-hack.com\nThanks to Kitsune for his original PSO2Proxy project\nThanks to every IG member past and present.\nThanks to my family.\n";
            MessageBox.Show(credit_text, "Credits");
        }

        //Manual packet send to server
        private void button1_Click(object sender, EventArgs e)
        {
            //send to server
            string packet = textBox1.Text;
            string cleaned_packet = packet.Replace("0x", "").Replace(" ", "");
            byte[] packet_fix = Encoding.ASCII.GetBytes(cleaned_packet);
            shipServer.m_vClient.SendToServer(packet_fix);
            MessageBox.Show("Packet was sent to server!", "Packet");
        }

        //Manual packet send to client
        private void button2_Click(object sender, EventArgs e)
        {
            //send to client
            string packet = textBox1.Text;
            string cleaned_packet = packet.Replace("0x", "").Replace(" ", "");
            byte[] packet_fix = Encoding.ASCII.GetBytes(cleaned_packet);
            shipServer.m_vClient.SendToClient(packet_fix);
            MessageBox.Show("Packet was sent to client!", "Packet");
        }

        //Clear packet log
        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            File.WriteAllText(Directory.GetCurrentDirectory() + @"\log\packet_log.txt", String.Empty);
        }

        //Clear console log
        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            File.WriteAllText(Directory.GetCurrentDirectory() + @"\log\log.txt", String.Empty);
        }

        //Remove text from packet sender on click
        private void textBox1_Click(object sender, EventArgs e)
        {
            /*
            if (textBox1.Text.Trim() != "" || textBox1.Text != null)
            {
                textBox1.Text = "";
            }
            */
        }

    }
}
