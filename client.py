import configparser
import socket

server_hostname = '127.0.0.1'
server_port = 12205

s = socket.socket()
host = server_hostname
port = int(server_port)
s.connect((host, port))
connection_info = s.recv(1024)

#buffer_size = 2048
#s.recv(buffer_size, 1)
